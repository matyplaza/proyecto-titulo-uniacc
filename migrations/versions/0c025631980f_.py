"""empty message

Revision ID: 0c025631980f
Revises: f97b583d5ab2
Create Date: 2017-11-30 12:13:32.478021

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0c025631980f'
down_revision = 'f97b583d5ab2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('campanas',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nombre', sa.String(length=60), nullable=True),
    sa.Column('descripcion', sa.String(length=140), nullable=True),
    sa.Column('hashtag', sa.String(length=60), nullable=True),
    sa.Column('screen_name', sa.String(length=60), nullable=True),
    sa.Column('fecha_inicio', sa.DateTime(), nullable=True),
    sa.Column('fecha_termino', sa.DateTime(), nullable=True),
    sa.Column('estado', sa.String(length=60), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_campanas_descripcion'), 'campanas', ['descripcion'], unique=False)
    op.create_index(op.f('ix_campanas_estado'), 'campanas', ['estado'], unique=False)
    op.create_index(op.f('ix_campanas_fecha_inicio'), 'campanas', ['fecha_inicio'], unique=False)
    op.create_index(op.f('ix_campanas_fecha_termino'), 'campanas', ['fecha_termino'], unique=False)
    op.create_index(op.f('ix_campanas_hashtag'), 'campanas', ['hashtag'], unique=False)
    op.create_index(op.f('ix_campanas_nombre'), 'campanas', ['nombre'], unique=False)
    op.create_index(op.f('ix_campanas_screen_name'), 'campanas', ['screen_name'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_campanas_screen_name'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_nombre'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_hashtag'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_fecha_termino'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_fecha_inicio'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_estado'), table_name='campanas')
    op.drop_index(op.f('ix_campanas_descripcion'), table_name='campanas')
    op.drop_table('campanas')
    # ### end Alembic commands ###
