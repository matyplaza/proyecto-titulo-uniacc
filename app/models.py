# -*- coding: utf-8 -*-
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login_manager


class User(UserMixin, db.Model):

    # Crea la tabla User
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True)
    username = db.Column(db.String(60), index=True, unique=True)
    nombre = db.Column(db.String(60), index=True)
    apellido = db.Column(db.String(60), index=True)
    password_hash = db.Column(db.String(128))
    es_admin = db.Column(db.Boolean, default=False)
    activo = db.Column(db.Boolean, default=True)
    campana = db.relationship("Campana", back_populates="user")

    @property
    def password(self):
        # Previene que se acceda al password
        raise AttributeError('El password no es un atributo de lectura')

    @password.setter
    def password(self, password):
        # Genera un password utilizando una funcion de hash
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        # Verifica que el password corresponda
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<Usuario: {}>'.format(self.id)


class Campana(db.Model):

    __tablename__ = 'campanas'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(60), index=True)
    descripcion = db.Column(db.String(140), index=True)
    hashtag = db.Column(db.String(60), index=True)
    screen_name = db.Column(db.String(60), index=True)
    fecha_inicio = db.Column(db.DateTime, index=True)
    fecha_termino = db.Column(db.DateTime, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    finalizacion_notificada = db.Column(db.Boolean, default=False)
    estado = db.Column(db.String(60), index=True)
    resultados = db.relationship('ResultadosCampanas', backref='campana')
    user = db.relationship('User', back_populates='campana')

    def __repr__(self):
        return '<Campana:{}>'.format(self.nombre)


class ResultadosCampanas(db.Model):

    __tablename__ = 'resultados_campanas'
    id = db.Column(db.Integer, primary_key=True)
    campana_id = db.Column(db.Integer, db.ForeignKey('campanas.id'))
    emocion = db.Column(db.String(60), index=True)
    genero = db.Column(db.String(60), index=True)
    cantidad = db.Column(db.Integer, index=True)

    def __repr__(self):
        return '<Resultado Campana:{}>'.format(self.id)


# Define user_loader
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
