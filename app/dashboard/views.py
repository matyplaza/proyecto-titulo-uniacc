# -*- coding: utf-8 -*-
from flask import render_template, abort
from flask_login import login_required, current_user
from app.utils import Utils
from . import dashboard
from ..models import Campana
from .. import db
from sqlalchemy.sql.functions import func
from flask_sqlalchemy import get_debug_queries
from app.persistencia import mysqlutils
from datetime import datetime



@dashboard.route('/dashboard')
@login_required
def index():
    mysql = mysqlutils.Utils()

    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()

    # Se cargan todos los menus
    menu = Utils.init_menu()
    # Se definen los menus activos
    menu['index']['estado'] = 'active'

    # Estadísticas generales
    mysql = mysqlutils.Utils()
    total_campanas = mysql.get_total_campanas()
    estadisticas_generales = {'registradas': 0, 'programada': 0, 'en_curso': 0, 'finalizada': 0}
    for c in total_campanas:
        estadisticas_generales['registradas'] += c.cantidad
        if c.estado != "procesando_resultados":
            estadisticas_generales[c.estado] = c.cantidad

    # Campañas en curso
    campanas_en_curso = mysql.get_campanas_en_curso()

    # AAS en campañas
    # Totales acumulados
    datos_radar = mysql.get_totales_acumulados(False, True, False, True)

    # Evolución por campaña
    datos_line = mysql.get_evolucion_campanas()

    return render_template('dashboard/index.html',
                           current_user=current_user,
                           datos_radar=datos_radar,
                           datos_line=datos_line,
                           estadisticas_generales=estadisticas_generales,
                           campanas_en_curso=campanas_en_curso,
                           menu=menu,
                           navbar_data=navbar_data
                           )
