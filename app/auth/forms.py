# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    usuario = StringField('Usuario',
                          validators=[DataRequired(u"Debe ingresar el usuario")],
                          render_kw={"placeholder": "Usuario"})
    clave = PasswordField('Clave',
                          validators=[DataRequired(u"Debe ingresar la clave")],
                          render_kw={"placeholder": "Clave"})
    submit = SubmitField('Ingresar')
