# -*- coding: utf-8 -*-
from flask import Flask, render_template, flash, redirect, url_for
from flask_login import login_required, login_user, logout_user, current_user
from werkzeug.security import generate_password_hash

from .forms import LoginForm
from . import auth
from .. models import User


@auth.route("/", methods=['GET', 'POST'])
def login():
    if current_user.get_id() is not None:
        return redirect(url_for('dashboard.index'))
    form = LoginForm()
    if form.validate_on_submit():
        usuario = User.query.filter_by(username=form.usuario.data, activo=1).first()
        if usuario is not None and usuario.verify_password(form.clave.data):
            # Se loguea al usuario
            login_user(usuario)

            # redirect to the dashboard page after login
            return redirect(url_for('dashboard.index'))

        # Si los datos son incorrectos
        else:
            flash(u'Usuario o clave inválidos', 'danger')
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash(u'Ha cerrado sesión', 'success')

    # Redirige al login
    return redirect(url_for('auth.login'))
