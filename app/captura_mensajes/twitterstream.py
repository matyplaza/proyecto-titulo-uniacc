# -*- coding: utf-8 -*-
from config import *
import tweepy
import json
import urllib3
from app.persistencia import elasticsearchutils
from flask import Flask
import logging
from app.aas.preprocesamiento import Limpieza
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()

urllib3.disable_warnings()

aplicacion_captura = Flask(__name__)

# Autenticación contra el API de Twitter
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth)

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, data):
        # Se intenta decodificar el Json obtenido
        try:
            tweet_original = json.loads(data)
        except Exception as e:
            logger.error(e)
            return True
        prepro = Limpieza()
        # Si es procesable
        if prepro.es_procesable(tweet_original):
            # Se prepara el json con el resumen de la info que nos interesa almacenar.
            json_compacto = prepro.crear_json_compacto(tweet_original)
            # Se almacena en ES.
            els = elasticsearchutils.Utils()
            els.add_tweet(json_compacto)
        return True

    # Si el API nos devuelve un error en la consulta.
    def on_error(self, status_code):
        logger.error(u"Se encontró un error con el código de estado: %s", status_code)
        return True

    # Si se genera un Timeout.
    def on_timeout(self):
        logger.error(u"Timeout...")
        return True


def captura():
    # Se ejecuta la llamada al API, con el filtro de geolocalización.
    sapi = tweepy.streaming.Stream(auth, CustomStreamListener())
    sapi.filter(locations=ciudad)

