# -*- coding: utf-8 -*-
import os

# Ruta utilizada para el almacenamiento
ruta_base= os.path.abspath("")

# Definición de parámetro de zona geográfica desde donde se emiten los tweets, utilizando las herramientas que proporciona Google (https://google-developers.appspot.com/maps/documentation/utils/geocoder).
# Se define Santiago por defecto.
ciudad = [-70.832882,-33.674188,-70.426781,-33.271387]

# Llaves para el API de Twitter:
consumer_key=os.getenv('TWITTER_CONSUMER_KEY')
consumer_secret=os.getenv('TWITTER_CONSUMER_SECRET')
access_key=os.getenv('TWITTER_ACCESS_KEY')
access_secret=os.getenv('TWITTER_ACCESS_SECRET')
