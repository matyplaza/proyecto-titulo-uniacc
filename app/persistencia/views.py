# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, url_for
from flask_login import login_required, current_user
from app.utils import Utils
from . import elasticsearchutils
from . import mysqlutils
from . import persistencia

import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()

mysql = mysqlutils.Utils()

@persistencia.route("/datos-almacenados")
@persistencia.route("/datos-almacenados/<string:accion>")
@login_required
def datos_almacenados(accion=None, archivo=None):
    # Es una función de administrador
    Utils.check_admin()

    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    # Se cargan todos los menus
    menu = Utils.init_menu()
    # Se definen los menus activos
    menu['treeview_elasticsearch']['estado'] = 'active'
    menu['treeview_elasticsearch']['datos_almacenados']['estado'] = 'active'

    resultado_accion = None
    els = elasticsearchutils.Utils()
    if accion == "ultimos":
        logger.info(u"Buscando los últimos 50 registrados ")

        resultado_search = els.search_last()
        resultado_accion = {
            'total': resultado_search['total'],
            'listado': resultado_search['hits']
        }
    elif accion == "delete-all":
        res = els.delete_all()
        if res:
            flash(u"Datos eliminados de Elasticsearch", "success")
        else:
            flash(u"Se produjo un error en la eliminación", "error")
        return redirect(url_for('dashboard.index'))
    elif accion == "import-all":
        res = els.importar()
        if res:
            flash(u"Se procesó un total de " + str(res['cantidad_tweets']) + " en " + str(
                res['tiempo_ejecucion']) + " segundos", "success")
        else:
            flash(u"Se produjo un error en la importación", "error")
        return redirect(url_for('dashboard.index'))
    else:
        accion = "No hay acción"

    return render_template('persistencia/elasticsearch.html',
                           title='Es la página de Elasticsearch',
                           contenido='',
                           mostrar_resultado=True,
                           accion=accion.decode('utf-8'),
                           resultado_accion=resultado_accion,
                           current_user=current_user,
                           menu=menu,
                           navbar_data=navbar_data
                           )
