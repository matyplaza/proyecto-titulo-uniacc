# -*- coding: utf-8 -*-
from ..models import Campana, ResultadosCampanas
from .. import db
from sqlalchemy import and_
from datetime import datetime
from app.utils import EstadosCampanas
from flask_login import current_user
from sqlalchemy.sql.functions import func
import json
from app.aas.procesamiento import Analisis
import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()

class Utils:
    def __init__(self):
        aas = Analisis()
        self.sentimientos = aas.sentimientos

    def get_navbar_data(self):
        navbar = {}

        por_notificar = self.get_campanas_por_notificar()
        navbar['finalizadas'] = {}
        navbar['finalizadas']['cantidad'] = len(por_notificar)
        navbar['finalizadas']['detalle'] = por_notificar

        en_curso = self.get_campanas_en_curso()
        navbar['en_curso'] = {}
        navbar['en_curso']['cantidad'] = len(en_curso)
        navbar['en_curso']['detalle'] = en_curso
        return navbar

    def get_total_campanas(self):
        campanas = db.session.query(Campana.estado, func.count(Campana.estado).label('cantidad')).filter(
            Campana.user_id == current_user.id).group_by(Campana.estado).all()
        return campanas

    # Obtiene el listado de campañas aptas para la generación de resultados finales.
    def get_campanas_procesables(self):

        campanas = Campana.query.filter(
                        and_(Campana.fecha_termino <= datetime.now(),
                             Campana.estado == EstadosCampanas().PROCESANDO_RESULTADOS)).all()
        return campanas

    def get_campanas_en_curso(self, user_id=None):
        campanas_en_curso = {}
        if user_id is None:
            user_id = current_user.id
        campanas = Campana.query.filter(
            and_(Campana.estado == EstadosCampanas().EN_CURSO,
                 Campana.user_id == user_id)).order_by(Campana.fecha_termino).all()

        for campana in campanas:
            dias_totales = (campana.fecha_termino - campana.fecha_inicio).days
            dias_restantes = (campana.fecha_termino - datetime.now()).days
            porcentaje_avance = ((dias_totales - dias_restantes) * 100) / dias_totales if dias_totales > 0 else 100
            campanas_en_curso[campana.id] = {}
            campanas_en_curso[campana.id]['nombre'] = campana.nombre
            campanas_en_curso[campana.id]['dias_restantes'] = str(dias_restantes) if dias_restantes >= 0 else '0'
            campanas_en_curso[campana.id]['porcentaje_avance'] = str(porcentaje_avance) if porcentaje_avance <= 100 else '100'

        return campanas_en_curso

    def get_campanas_por_notificar(self, user_id=None):
        if user_id is None:
            user_id = current_user.id
            campanas_por_notificar = Campana.query.filter(
            and_(Campana.estado == EstadosCampanas().FINALIZADA,
                 Campana.finalizacion_notificada == 0,
                 Campana.user_id == user_id)).order_by(Campana.fecha_termino).all()

        return campanas_por_notificar

    def get_totales_acumulados(self, campana_id=False, porcentaje=False, genero=False, finalizadas=False):
        resultado_final = [0,0,0,0,0,0]
        # Se realiza la consulta a MySQL
        query_filters = []
        query = Campana.query.join(Campana.resultados).filter()
        if campana_id is not False:
            query_filters.append(Campana.id == campana_id)
        else:
            query_filters.append(Campana.user_id == current_user.id)

        if finalizadas is True:
            query_filters.append(Campana.estado == EstadosCampanas().FINALIZADA)

        query = query.filter(*query_filters)
        totales_acumulados = query.all()

        # Se inicializa la estructura que contendrá los datos
        datos = {'totales': {
            'todos': 0, 'male': 0, 'female': 0, 'unknown': 0
        }, 'detalle': {
            'todos': {}, 'male': {}, 'female': {}, 'unknown': {}
        }}
        for emocion in self.sentimientos:
            datos['detalle']['todos'][emocion] = 0
            datos['detalle']['male'][emocion] = 0
            datos['detalle']['female'][emocion] = 0
            datos['detalle']['unknown'][emocion] = 0

        # Se realizan los conteos
        for campana in totales_acumulados:
            for resultado in campana.resultados:
                datos['totales']['todos'] += int(resultado.cantidad)
                datos['totales'][resultado.genero] += int(resultado.cantidad)
                datos['detalle']['todos'][resultado.emocion] += int(resultado.cantidad)
                datos['detalle'][resultado.genero][resultado.emocion] += int(resultado.cantidad)

        if int(datos['totales']['todos']) == 0 or ( (genero is not False) and (int(datos['totales'][str(genero)]) == 0) ):
            return resultado_final

        # Se prepara el resultado a entregar según los parámetros recibidos
        if (porcentaje is False and genero is False) or datos['totales']['todos'] == 0:
            resultado_final = [int(datos['detalle']['todos']['alegria']),
                               int(datos['detalle']['todos']['ira']),
                               int(datos['detalle']['todos']['miedo']),
                               int(datos['detalle']['todos']['asco']),
                               int(datos['detalle']['todos']['sorpresa']),
                               int(datos['detalle']['todos']['tristeza'])]
        elif porcentaje is False:
            resultado_final = [int(datos['detalle'][genero]['alegria']),
                               int(datos['detalle'][genero]['ira']),
                               int(datos['detalle'][genero]['miedo']),
                               int(datos['detalle'][genero]['asco']),
                               int(datos['detalle'][genero]['sorpresa']),
                               int(datos['detalle'][genero]['tristeza'])]
        elif porcentaje is True and genero is False:
            resultado_final = [int((datos['detalle']['todos']['alegria']) * 100 / (datos['totales']['todos'])),
                               int((datos['detalle']['todos']['ira']) * 100 / (datos['totales']['todos'])),
                               int((datos['detalle']['todos']['miedo']) * 100 / (datos['totales']['todos'])),
                               int((datos['detalle']['todos']['asco']) * 100 / (datos['totales']['todos'])),
                               int((datos['detalle']['todos']['sorpresa']) * 100 / (datos['totales']['todos'])),
                               int((datos['detalle']['todos']['tristeza']) * 100 / (datos['totales']['todos']))]
        elif porcentaje is True and genero is not False:
            resultado_final = [int((datos['detalle'][genero]['alegria']) * 100 / (datos['totales'][genero])),
                               int((datos['detalle'][genero]['ira']) * 100 / (datos['totales'][genero])),
                               int((datos['detalle'][genero]['miedo']) * 100 / (datos['totales'][genero])),
                               int((datos['detalle'][genero]['asco']) * 100 / (datos['totales'][genero])),
                               int((datos['detalle'][genero]['sorpresa']) * 100 / (datos['totales'][genero])),
                               int((datos['detalle'][genero]['tristeza']) * 100 / (datos['totales'][genero]))]
        else:
            return resultado_final

        return resultado_final

    def get_evolucion_campanas(self):

        # Se realiza la consulta a MySQL
        campanas = Campana.query.join(Campana.resultados).filter(
            Campana.estado == EstadosCampanas().FINALIZADA,
            Campana.user_id == current_user.id).order_by(Campana.fecha_termino).all()

        respuesta = {'labels': [], 'emociones': {
            'Alegria': [],
            'Ira': [],
            'Miedo': [],
            'Asco': [],
            'Sorpresa': [],
            'Tristeza': []
        }}

        # Se realizan los conteos
        for campana in campanas:
            # Se inicializa la estructura que contendrá los datos
            total = 0
            datos = {}
            for emocion in self.sentimientos:
                datos[emocion] = 0

            for resultado in campana.resultados:
                total += int(resultado.cantidad)
                datos[resultado.emocion] += int(resultado.cantidad)

            total = total if total > 0 else 100

            respuesta['labels'].append(campana.nombre)
            respuesta['emociones']['Alegria'].append(int(datos['alegria'])*100/total)
            respuesta['emociones']['Ira'].append(int(datos['ira'])*100/total)
            respuesta['emociones']['Miedo'].append(int(datos['miedo'])*100/total)
            respuesta['emociones']['Asco'].append(int(datos['asco'])*100/total)
            respuesta['emociones']['Sorpresa'].append(int(datos['sorpresa'])*100/total)
            respuesta['emociones']['Tristeza'].append(int(datos['tristeza'])*100/total)

        return json.dumps(respuesta)

    def cambiar_estados_campanas(self):
        en_curso = Campana.query.filter(
                            and_(datetime.now() >= Campana.fecha_inicio,
                                 datetime.now() < Campana.fecha_termino,
                                 Campana.estado != EstadosCampanas().FINALIZADA,
                                 Campana.estado != EstadosCampanas().EN_CURSO))\
            .update({Campana.estado: EstadosCampanas().EN_CURSO})
        procesando_resultados = Campana.query.filter(
                            and_(datetime.now() >= Campana.fecha_termino),
                            Campana.estado != EstadosCampanas().FINALIZADA,
                                 Campana.estado != EstadosCampanas().PROCESANDO_RESULTADOS)\
            .update({Campana.estado: EstadosCampanas().PROCESANDO_RESULTADOS})

        db.session.commit()

        actualizadas = {'en_curso': en_curso, 'procesando_resultados': procesando_resultados}
        return actualizadas

    def notificacion_leida(self, id):
        actualizada = Campana.query.filter(
            and_(Campana.id == id, Campana.estado == EstadosCampanas().FINALIZADA)).\
            update({Campana.finalizacion_notificada: True})
        db.session.commit()

        return actualizada

    def registrar_resultado_campana(self, campana_id, emocion, genero, cantidad):
        resultado = ResultadosCampanas(campana_id=campana_id,
                                       emocion=emocion,
                                       genero=genero,
                                       cantidad=cantidad)
        try:
            db.session.add(resultado)
            db.session.commit()
            logger.info(u"Se ha creado un nuevo registro de resultados para la campaña <id:%d>", campana_id)
        except:
            logger.error(u"ERROR al crear un nuevo registro de resultados para la campaña <id:%d>", campana_id)
