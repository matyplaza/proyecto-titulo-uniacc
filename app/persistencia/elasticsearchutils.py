# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from app.aas import procesamiento
import json
import os
from time import time
from app.aas.preprocesamiento import Limpieza

import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()


class Utils:
    def __init__(self):
        pass

    # Consultar documentación en:
    # http://elasticsearch-dsl.readthedocs.io/en/latest/search_dsl.html
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/full-text-queries.html
    def busqueda_estandar(self, parametros):
        es = Elasticsearch()
        resultados = {}
        comunas = {}
        totales = {}

        # Se definen los parámetros a utilizar en la búsqueda
        hashtag = parametros['hashtag'] if parametros['hashtag'] is not None else ''
        screen_name = parametros['screen_name'] if parametros['screen_name'] is not None else ''
        fecha_inicio = parametros['fecha_inicio_es']
        fecha_termino = parametros['fecha_termino_es']

        # Se efectúa la búsqueda en ES, considerando un límite de 10 mil resultados y la exclusión de geolocalización no determinada para una comuna
        res = es.search(
            index="twitter",
            body={
                "from": 0,
                "size": 10000,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "bool": {
                                    "should": [
                                        {
                                            "match": {
                                                "entities.user_mentions.screen_name": {
                                                    "query": screen_name,
                                                    "operator": "or",
                                                    "zero_terms_query": "none"
                                                }
                                            }
                                        },
                                        {
                                            "match": {
                                                "entities.hashtags.text": {
                                                    "query": hashtag,
                                                    "operator": "or",
                                                    "zero_terms_query": "none"
                                                }
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                               "match": {
                                    "place.name": {
                                        "query": u'Santiago  Independencia  Conchalí  Huechuraba  Recoleta '
                                                 u'Providencia  Vitacura  Lo Barnechea  Las Condes  Ñuñoa '
                                                 u'La Reina  Macul  Peñalolén  La Florida  San Joaquín '
                                                 u'La Granja  La Pintana  San Ramón  San Miguel  La Cisterna '
                                                 u'El Bosque  Pedro Aguirre Cerda  Lo Espejo  Estación Central '
                                                 u'Cerrillos  Maipú  Quinta Normal  Lo Prado  Pudahuel '
                                                 u'Cerro Navia  Renca  Quilicura  Puente Alto '
                                                 u'San José de Maipo  Pirque  San Bernardo  Calera de Tango '
                                                 u'Colina  Lampa  Til-Til  Talagante  Melipilla  Peñaflor '
                                                 u'Padre Hurtado  El Monte  Curacaví  Isla de Maipo '
                                                 u'María Pinto  San Pedro  Alhué  Buin  Paine',
                                        "operator": "or"
                                    }
                                }
                            }
                        ],
                        "filter": [
                            {
                                "range": {
                                    "created_at": {
                                        "gte": fecha_inicio,
                                        "lte": fecha_termino,
                                        "time_zone": "-03:00"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        )

        # Se prepara la estructura del resultado entregado para dibujar en la vista
        for attribute, value in res['hits'].iteritems():
            if attribute == 'hits':
                for x in value:
                    for attr, val in x.iteritems():
                        if attr == '_source':
                            # Conteo de sentimientos por comuna
                            if comunas.get(val['place']['name']) is None:
                                comunas[val['place']['name']] = {}
                                comunas[val['place']['name']]['sentimientos'] = {}
                                comunas[val['place']['name']]['sentimientos'][val['sentimiento']] = 1
                            elif comunas[val['place']['name']]['sentimientos'].get(val['sentimiento']) is None:
                                comunas[val['place']['name']]['sentimientos'][val['sentimiento']] = 1
                            else:
                                comunas[val['place']['name']]['sentimientos'][val['sentimiento']] += 1

                            # Conteo general de sentimientos
                            if totales.get(val['sentimiento']) is None:
                                totales[val['sentimiento']] = 1
                            else:
                                totales[val['sentimiento']] += 1

                            # Total de mensajes por comuna
                            if comunas[val['place']['name']].get('total') is None:
                                comunas[val['place']['name']]['total'] = 1
                            else:
                                comunas[val['place']['name']]['total'] += 1

            # Suma total de mensajes
            if attribute == 'total':
                totales['suma_total'] = value

        # Datos se entregan como resultado
        resultados['comunas'] = comunas
        resultados['totales'] = totales
        return resultados

    def add_tweet(self, doc):
        es = Elasticsearch()
        aas = procesamiento.Analisis()
        doc['sentimiento'] = aas.get_sentimiento(doc['text'])
        # Agrega a ES
        logger.info(u"Tweet: %s", doc)

        res = es.index(index="twitter", doc_type='tweet', body=doc)
        return res

    def search_all(self):
        es = Elasticsearch()
        # Efectúa la búsqueda
        res = es.search(index="twitter", body={"from" : 0,
                                                  "size" : 10000,
                                                  "query": {"match_all": {}}})
        return res['hits']

    def search_last(self):
        es = Elasticsearch()
        # Efectúa la búsqueda de los últimos 50 registros
        body_search = {
          "query": {
            "match_all": {}
          },
          "size": 50,
          "sort": [
            {
              "created_at": {
                "order": "desc"
              }
            }
          ]
        }
        res = es.search(index="twitter", body=body_search)

        return res['hits']

    def delete_all(self):
        # Borra todo en ES
        es = Elasticsearch()
        res = es.delete_by_query(index="twitter", body={"query": {"match_all": {}}})
        return res

    def importar(self):
        resultado = {'cantidad_tweets': 0, 'tiempo_ejecucion': 0}
        tiempo_inicial = time()
        ruta_base = os.path.abspath("")
        contador = 0
        registro = open(ruta_base + '/importacion_masiva/importados.registro', "w")

        for root, dirs, files in os.walk(ruta_base + '/importacion_masiva'):
            for name in files:
                ext = os.path.splitext(name)[-1].lower()
                if ext == '.txt':
                    ruta_completa = os.path.join(root, name)
                    logger.info(u"Importando el archivo: %s", name)
                    with open(ruta_completa, 'r') as archivo:
                        tweets = archivo.readlines()
                        for tweet in tweets:
                            decoded = json.loads(tweet)
                            prepro = Limpieza()
                            if prepro.es_procesable(decoded):
                                contador += 1
                                json_compacto = prepro.crear_json_compacto(decoded)
                                logger.info(u"Importando Tweet N° %d", contador)
                                # Se indexa el tweet ya procesado, en Elasticsearch
                                self.add_tweet(json_compacto)
                    registro.write(str(name))
                    registro.write('\n')

        tiempo_final = time()
        resultado['cantidad_tweets'] = contador
        resultado['tiempo_ejecucion'] = tiempo_final - tiempo_inicial

        registro.write('Total importado: ' + str(resultado['cantidad_tweets']) + ' / Tiempo ejecucion: ' + str(resultado['tiempo_ejecucion']) )
        registro.write('\n')
        registro.close()
        logger.info(u"Resultado importación: %s", resultado)

        return resultado

    def busqueda_heatmap(self, campana, emocion=False, genero=False):
        es = Elasticsearch()
        # Se definen los parámetros a utilizar en la búsqueda
        hashtag = campana.hashtag if campana.hashtag else ''
        screen_name = campana.screen_name if campana.screen_name else ''
        fecha_inicio = campana.fecha_inicio.strftime('%a %b %d %H:%M:%S +0000 %Y')
        fecha_termino = campana.fecha_termino.strftime('%a %b %d %H:%M:%S +0000 %Y')

        body_search = {
                "from": 0,
                "size": 10000,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "bool": {
                                    "should": [
                                        {
                                            "match": {
                                                "entities.user_mentions.screen_name": {
                                                    "query": screen_name,
                                                    "operator": "or",
                                                    "zero_terms_query": "none"
                                                }
                                            }
                                        },
                                        {
                                            "match": {
                                                "entities.hashtags.text": {
                                                    "query": hashtag,
                                                    "operator": "or",
                                                    "zero_terms_query": "none"
                                                }
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                               "match": {
                                    "place.name": {
                                        "query": u'Santiago  Independencia  Conchalí  Huechuraba  Recoleta '
                                                 u'Providencia  Vitacura  Lo Barnechea  Las Condes  Ñuñoa '
                                                 u'La Reina  Macul  Peñalolén  La Florida  San Joaquín '
                                                 u'La Granja  La Pintana  San Ramón  San Miguel  La Cisterna '
                                                 u'El Bosque  Pedro Aguirre Cerda  Lo Espejo  Estación Central '
                                                 u'Cerrillos  Maipú  Quinta Normal  Lo Prado  Pudahuel '
                                                 u'Cerro Navia  Renca  Quilicura  Puente Alto '
                                                 u'San José de Maipo  Pirque  San Bernardo  Calera de Tango '
                                                 u'Colina  Lampa  Til-Til  Talagante  Melipilla  Peñaflor '
                                                 u'Padre Hurtado  El Monte  Curacaví  Isla de Maipo '
                                                 u'María Pinto  San Pedro  Alhué  Buin  Paine',
                                        "operator": "or"
                                    }
                                }
                            }
                        ],
                        "filter": [
                            {
                                "range": {
                                    "created_at": {
                                        "gte": fecha_inicio,
                                        "lte": fecha_termino,
                                        "time_zone": "-03:00"
                                    }
                                }
                            }
                        ]
                    }
                }
            }

        if emocion is not False:
            body_search['query']['bool']['must'].append({"match": {"sentimiento": {"query": emocion}}})

        if genero is not False:
            body_search['query']['bool']['must'].append({"match": {"user.gender": {"query": genero}}})

        # Se efectúa la búsqueda en ES, considerando la exclusión de geolocalización no deseada
        res = es.search(
            index="twitter",
            body=body_search
        )

        return res
