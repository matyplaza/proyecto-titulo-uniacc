from flask import abort
from flask_login import current_user

class Utils:
    def __init__(self):
        pass

    @staticmethod
    def init_menu():
        menu = {
            'index': {
                'estado': ''
            },
            'treeview_campanas': {
                'estado': '',
                'crear': {
                    'estado': ''
                },
                'consultar': {
                    'estado': ''
                },
                'analisis': {
                    'estado': ''
                }
            },
            'treeview_elasticsearch': {
                'estado': '',
                'datos_almacenados': {
                    'estado': ''
                }
            },
            'treeview_aas': {
                'estado': '',
                'entrenar_modelo': {
                    'estado': ''
                },
                'probar_modelo': {
                    'estado': ''
                }
            },
            'treeview_admin': {
                'estado': '',
                'usuarios': {
                    'estado': ''
                }
            }
        }
        return menu

    @staticmethod
    def check_admin():
        if not current_user.es_admin:
            abort(403)

class EstadosCampanas:
    def __init__(self):
        self.PROGRAMADA = 'programada'
        self.EN_CURSO = 'en_curso'
        self.PROCESANDO_RESULTADOS = 'procesando_resultados'
        self.FINALIZADA = 'finalizada'
