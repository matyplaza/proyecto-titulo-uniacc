# -*- coding: utf-8 -*-
from flask import render_template
from flask_login import login_required, current_user
from .forms import ProbarModeloForm
from app.utils import Utils
from . import procesamiento
from . import aas
from app.persistencia import mysqlutils

mysql = mysqlutils.Utils()

@aas.route("/aas/entrenar-modelo")
@login_required
def entrenar_modelo():
    Utils.check_admin()
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    # Se cargan todos los menus
    menu = Utils.init_menu()
    # Se definen los menus activos
    menu['treeview_aas']['estado'] = 'active'
    menu['treeview_aas']['entrenar_modelo']['estado'] = 'active'

    pro = procesamiento.Analisis()
    clf, reporte, matriz_confusion = pro.entrenar_modelo()
    estadisticas = pro.procesar_reporte(reporte)
    sentimientos = pro.sentimientos

    return render_template('aas/entrenar-modelo.html',
                           clf=clf,
                           estadisticas=estadisticas,
                           matriz_confusion=matriz_confusion,
                           current_user=current_user,
                           sentimientos=sentimientos,
                           menu=menu,
                           navbar_data=navbar_data
                           )


@aas.route("/aas/probar-modelo", methods=['GET', 'POST'])
@login_required
def probar_modelo():
    Utils.check_admin()
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    # Se cargan todos los menus
    menu = Utils.init_menu()
    # Se definen los menus activos
    menu['treeview_aas']['estado'] = 'active'
    menu['treeview_aas']['probar_modelo']['estado'] = 'active'

    form = ProbarModeloForm()
    mensaje = ""
    resultado = ""

    if form.validate_on_submit():
        pro = procesamiento.Analisis()
        mensaje = form.mensaje.data
        resultado = pro.probar_modelo(form.mensaje.data)
    return render_template('aas/probar-modelo.html',
                           form=form,
                           mensaje=mensaje,
                           resultado=resultado,
                           current_user=current_user,
                           menu=menu,
                           navbar_data=navbar_data
                           )
