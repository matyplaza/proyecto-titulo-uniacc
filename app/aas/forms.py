# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

class ProbarModeloForm(FlaskForm):
    mensaje = StringField('mensaje',
                          validators=[DataRequired("Debe ingresar el mensaje")],
                          render_kw={"placeholder": "Mensaje"})