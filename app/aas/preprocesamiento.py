# -*- coding: utf-8 -*-
import re
import nltk
from nltk.corpus import stopwords
from string import punctuation
from nltk.stem import SnowballStemmer
import langid
from langdetect import detect
import textblob
from gender_detector import GenderDetector
import json
import unidecode

class Limpieza:
    def __init__(self):
        pass

    def detectar_genero(self, nombre):
        detector = GenderDetector('ar')
        try:
            genero = detector.guess(nombre)
            return genero
        except:
            return 'unknown'


    # Crea un documento con los datos que se almacenarán en la aplicación
    def crear_json_compacto(self, tweet_original):
        # Se detecta el género del usuario
        nombre_completo = tweet_original['user']['name'].partition(' ')
        genero = self.detectar_genero(nombre_completo[0])
        json_compacto = {}
        if 'coordinates' in tweet_original['coordinates']:
            json_compacto['coordinates'] = tweet_original['coordinates']['coordinates']
        else:
            json_compacto['coordinates'] = tweet_original['coordinates']
        json_compacto['created_at'] = tweet_original['created_at']
        json_compacto['entities'] = tweet_original['entities']
        json_compacto['favorite_count'] = tweet_original['favorite_count']
        json_compacto['lang'] = tweet_original['lang']
        json_compacto['place'] = {}
        json_compacto['place']['name'] = tweet_original['place']['name']
        json_compacto['place']['country'] = tweet_original['place']['country']
        json_compacto['retweet_count'] = tweet_original['retweet_count']
        json_compacto['text'] = tweet_original['text']
        json_compacto['user'] = {}
        json_compacto['user']['name'] = tweet_original['user']['name']
        json_compacto['user']['gender'] = genero
        json_compacto['user']['screen_name'] = tweet_original['user']['screen_name']
        json_compacto['user']['followers_count'] = tweet_original['user']['followers_count']
        return json_compacto

    # Determina si un tweet es procesable (tiene geolocalización al menos dentro de Chile y su mensaje está en español)
    def es_procesable(self, tweet):
        resultado = False
        if tweet['coordinates'] is not None and tweet['place']['country'] == "Chile" and tweet['lang'] == "es":
            resultado = self.confirmar_idioma_espanol(tweet['text'])
        return resultado

    # Confirma si un texto se encuentra en idioma español.
    # Basado en https://pybonacci.es/2015/11/24/como-hacer-analisis-de-sentimiento-en-espanol-2/
    def confirmar_idioma_espanol(self, mensaje):
        langid_idioma = langid.classify(mensaje)[0]
        langdetect_idioma = detect(mensaje)
        textblob_idioma = textblob.TextBlob(mensaje).detect_language()

        if langid_idioma == 'es' or langdetect_idioma == 'es' or textblob_idioma == 'es':
            return True
        else:
            return False

    # Se transforman las palabras tokenizadas a su raíz (por ejemplo: lindura, lindo, linda... tienen como raíz a "lind")
    def stem_tokens(self, tokens):
        # Se define el diccionario para el stemming en español
        stemmer = SnowballStemmer('spanish')
        stemmed = []
        for item in tokens:
            stemmed.append(stemmer.stem(item))
        return stemmed

    def full_clean_text(self, texto):
        # Se eliminan espacios en blanco iniciales y finales
        texto_limpio = texto.strip()
        # Se eliminan las URLs
        texto_limpio = re.sub(r'http\S+', '', texto_limpio)
        # Se eliminan las menciones a hashtags
        texto_limpio = re.sub(r'#\S+', '', texto_limpio)
        # Se eliminan las menciones a usuarios
        texto_limpio = re.sub(r'@\S+', '', texto_limpio)
        # Se eliminan las fechas
        texto_limpio = re.sub(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', '', texto_limpio)
        # Se eliminan los espacios en blanco duplicados
        texto_limpio = re.sub(r'\s+', ' ', texto_limpio)
        # Se eliminan los tildes
        texto_limpio = unidecode.unidecode(texto_limpio)

        return texto_limpio

    # Se convierte una cadena de texto en una lista, utilizando el tokenizador "nltk.word_tokenize"
    def tokenizar(self, mensaje, entrenamiento=False):
        # Se obtiene un listado con los signos de puntuación
        non_words = map(unicode, list(punctuation))
        # Al listado agregamos los signos de apertura de pregunta y exclamación en español
        non_words.extend(['¿', '¡'])
        # Al listado se le agregan los números del 0 al 9, convirtiéndolos en strings.
        non_words.extend(map(unicode, range(10)))

        # Si se está entrenando desde el corpus, se debe establecer el encoding del mensaje
        if entrenamiento:
            mensaje = unicode(mensaje, 'utf-8')

        # Se realiza una limpieza básica sobre el mensaje
        mensaje = self.full_clean_text(mensaje)

        # Se remueven las stopwords (palabras frecuentes pero sin valor sintáctico)
        spanish_stopwords = stopwords.words('spanish')
        no_consideradas = spanish_stopwords + non_words

        # Se obtiene un primer tokenizado sin stopwords ni signos de puntuacion
        pre_tokens = [i for i in nltk.word_tokenize(mensaje.lower()) if i not in no_consideradas]

        # Se obtiene un segundo tokenizado reducido a la raíz de la palabra
        tokens = self.stem_tokens(pre_tokens)

        # Se obtiene un tercer y final tokenizado, con el formato para ser utilizado por el modelo
        tokenizado = {}
        for t in tokens:
            tokenizado['%s' % t] = t

        return tokenizado
