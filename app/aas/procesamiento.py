# -*- coding: utf-8 -*-
import preprocesamiento
import os
import nltk
import random
from sklearn.svm import LinearSVC
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.metrics import classification_report, confusion_matrix
import pickle
import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()


class Analisis:
    def __init__(self):
        # Set de etiquetas correspondientes a las 6 emociones universales básicas de Ekman
        self.sentimientos = ['alegria', 'ira', 'miedo', 'asco', 'sorpresa', 'tristeza']

    # Realiza una predicción del sentimiento existente en un mensaje.
    def get_sentimiento(self, mensaje):
        # Se carga el objeto que contiene al modelo predictivo.
        with open('modelo.pkl', 'rb') as f:
            clf = pickle.load(f)
        # El mensaje se tokeniza
        prepro = preprocesamiento.Limpieza()
        tokenizado = prepro.tokenizar(mensaje, False)
        # Se predice la emoción según el modelo
        prediccion = clf.classify(tokenizado)
        return self.sentimientos[prediccion]


    # Basado en https://glowingpython.blogspot.cl/2013/07/combining-scikit-learn-and-ntlk.html
    def entrenar_modelo(self):
        nltk.download("stopwords")
        nltk.download("punkt")
        ruta_base = os.path.abspath("")
        prepro = preprocesamiento.Limpieza()

        featuresets = []
        contador = 1
        with open(ruta_base + '/corpus.txt', 'r') as archivo:
            logger.info(u"Entrenando Modelo......")
            tweets = archivo.readlines()
            for tweet in tweets:
                tweet_procesado = tweet.partition(' ')
                sentimiento = tweet_procesado[0].lower()
                featuresets.append((prepro.tokenizar(tweet_procesado[2], True), self.sentimientos.index(sentimiento)))
                logger.info(u"Analizando el Tweet N° %d", contador)
                contador = contador + 1

        # Después de la extracción, se obtiene el set de entrenamiento y el de prueba
        random.shuffle(featuresets)
        # Se usará el 10% del corpus para las pruebas
        size = int(len(featuresets) * .1)
        train = featuresets[size:]
        test = featuresets[:size]

        # Se instancia el modelo que implementa el clasificador usando la interface scikitlearn que provee NLTK y se entrena:
        # SVM con un Kernel Lineal y parámetros por defecto
        clf = SklearnClassifier(LinearSVC(
            penalty='l2',
            loss='squared_hinge',
            dual=False,
            tol=0.0001,
            C=0.03,
            multi_class='ovr',
            fit_intercept=True,
            intercept_scaling=1,
            verbose=0,
            random_state=None,
            max_iter=1000
        ))
        clf.train(train)

        # Para utilizar el método classify_many, se organiza el set de prueba en dos listas (datos de entrenamiento y etiquetas)
        test_skl = []
        t_test_skl = []
        for d in test:
            test_skl.append(d[0])
            t_test_skl.append(d[1])

        # Se ejecuta el clasificador en el set de prueba
        p = clf.classify_many(test_skl)

        # Se imprime el modelo obtenido
        logger.info(u"Modelo obtenido:")
        logger.info(clf)

        # Se imprime el reporte con los resultados
        reporte = classification_report(t_test_skl, p, target_names=self.sentimientos)
        logger.info(u"Reporte de Clasificación:")
        logger.info(reporte)

        matriz_confusion = confusion_matrix(t_test_skl, p)
        logger.info(u"Matriz de Confusión:")
        logger.info(matriz_confusion)

        # Se utiliza la persistencia de modelo
        with open('modelo.pkl', 'wb') as f:
            pickle.dump(clf, f)

        return clf, reporte, matriz_confusion

    # Prueba el modelo, realizando la predicción de un mensaje ingresado vía formulario web.
    def probar_modelo(self, mensaje):
        prepro = preprocesamiento.Limpieza()

        with open('modelo.pkl', 'rb') as f:
            clf = pickle.load(f)

        feature = prepro.tokenizar(mensaje, False)
        prediccion = clf.classify(feature)
        return self.sentimientos[prediccion].upper()

    # Procesa el reporte del clasificador (lo parsea y crea un JSON)
    def procesar_reporte (self, reporte):
        reporte = reporte.splitlines()
        res = []
        res.append([''] + reporte[0].split())
        for row in reporte[2:-2]:
            res.append(row.split())
        lr = reporte[-1].split()
        res.append([' '.join(lr[:3])] + lr[3:])
        return res




