# -*- coding: utf-8 -*-
import schedule
import time
import os
from app.reportes.reportes import Reporte
from app.persistencia import mysqlutils
from app import create_app
from app.utils import EstadosCampanas

config_name = os.getenv('FLASK_CONFIG')
app = create_app(config_name)
app.app_context().push()
from .. import db
import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()


# Modifica los estados de las campañas según sus parámetros configurados.
def cambiar_estados():
    mysql = mysqlutils.Utils()
    actualizadas = mysql.cambiar_estados_campanas()
    logger.info(u"%d Campañas actualizadas al estado: EN_CURSO", actualizadas['en_curso'])
    logger.info(u"%d Campañas actualizadas al estado: PROCESANDO_RESULTADOS", actualizadas['procesando_resultados'])


# Procesa las campañas en estado PROCESANDO_RESULTADOS para generar sus reportes y modificar su estado.
def generar_resultados():
    mysql = mysqlutils.Utils()
    # Se consulta por las campañas en condiciones de ser procesadas
    campanas = mysql.get_campanas_procesables()
    for campana in campanas:
        # Se procesa la campaña ya finalizada, generando los mapas de calor
        Reporte().generar_reportes(campana)

        # Se marca el estado como campaña procesada
        campana.estado = EstadosCampanas().FINALIZADA
        db.session.add(campana)
        db.session.commit()
        logger.info(u"Campaña <id:%d> cambió estado a FINALIZADA", campana.id)


def ejecutar():
    schedule.every(1).minutes.do(cambiar_estados)
    schedule.every(20).minutes.do(generar_resultados)

    while True:
        schedule.run_pending()
        time.sleep(1)
