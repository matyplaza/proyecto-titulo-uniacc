# -*- coding: utf-8 -*-
from flask import Flask, render_template
from flask_adminlte import AdminLTE
from flask_sqlalchemy import SQLAlchemy
from config import app_config
from flask_login import LoginManager
from flask_migrate import Migrate

db = SQLAlchemy()
login_manager = LoginManager()


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    AdminLTE(app)

    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = u"Debe iniciar sesión para acceder a esta página."
    login_manager.login_view = "auth.login"

    migrate = Migrate(app, db)

    from app import models

    from .auth import auth as auth__blueprint
    app.register_blueprint(auth__blueprint)

    from .dashboard import dashboard as dashboard__blueprint
    app.register_blueprint(dashboard__blueprint)

    from .campanas import campanas as campanas__blueprint
    app.register_blueprint(campanas__blueprint)

    from .persistencia import persistencia as persistencia__blueprint
    app.register_blueprint(persistencia__blueprint)

    from .aas import aas as aas__blueprint
    app.register_blueprint(aas__blueprint)

    from .admin import admin as admin__blueprint
    app.register_blueprint(admin__blueprint)

    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errores/403.html', title='Acceso Denegado'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errores/404.html', title='Página no Encontrada'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errores/500.html', title='Error del Servidor'), 500

    return app


if __name__ == '__main__':
    create_app().run()
