# -*- coding: utf-8 -*-
from subprocess import call
import sys
import os

import numpy as np
from PIL import Image, ImageDraw, ImageFont
from scipy.misc import imread, imsave
from colour import Color
from app.persistencia.elasticsearchutils import Utils
from app.persistencia.mysqlutils import Utils as Mysql
from app.aas.procesamiento import Analisis
from .. import db

import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()


class Reporte:
    def __init__(self):
        pass

    # Genera la totalidad de reportes según los parámetros de una campaña
    def generar_reportes(self, campana):
        es = Utils()
        msql = Mysql()

        # Se crea un mapa de calor global: "TODOS LOS TWEETS"
        datos = es.busqueda_heatmap(campana, False, False)
        nombre = str(campana.id) + "_todos"
        archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
        if cantidad_tweets > 0:
            self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, "Todas las Emociones")
        os.remove(archivo_coordenadas)

        # Se crea un mapa de calor, género masculino: "TODOS LOS TWEETS"
        datos = es.busqueda_heatmap(campana, False, 'male')
        nombre = str(campana.id) + "_todos_male"
        archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
        if cantidad_tweets > 0:
            self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, "Todas las Emociones (M)")
        os.remove(archivo_coordenadas)

        # Se crea un mapa de calor, género femenino: "TODOS LOS TWEETS"
        datos = es.busqueda_heatmap(campana, False, 'female')
        nombre = str(campana.id) + "_todos_female"
        archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
        if cantidad_tweets > 0:
            self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, "Todas las Emociones (F)")
        os.remove(archivo_coordenadas)

        # Se crea un mapa de calor, género indeterminado: "TODOS LOS TWEETS"
        datos = es.busqueda_heatmap(campana, False, 'unknown')
        nombre = str(campana.id) + "_todos_unknown"
        archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
        if cantidad_tweets > 0:
            self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, "Todas las Emociones (?)")
        os.remove(archivo_coordenadas)

        emociones = Analisis().sentimientos
        for emocion in emociones:

            # Se crea cada mapa de calor de la emoción y considerando todos los géneros
            datos = es.busqueda_heatmap(campana, emocion, False)
            nombre = str(campana.id) + "_" + emocion
            archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
            if cantidad_tweets > 0:
                self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, u"Emoción: " + str(emocion).upper())
            os.remove(archivo_coordenadas)

            # Se crea cada mapa de calor de la emoción y considerando el género masculino
            datos = es.busqueda_heatmap(campana, emocion, 'male')
            nombre = str(campana.id) + "_" + emocion + "_male"
            archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
            if cantidad_tweets > 0:
                self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, u"Emoción: " +
                                     str(emocion).upper() + u"(M)")
                msql.registrar_resultado_campana(campana.id, str(emocion).lower(), 'male', cantidad_tweets)
            os.remove(archivo_coordenadas)

            # Se crea cada mapa de calor de la emoción y considerando el género femenino
            datos = es.busqueda_heatmap(campana, emocion, 'female')
            nombre = str(campana.id) + "_" + emocion + "_female"
            archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
            if cantidad_tweets > 0:
                self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, u"Emoción: " +
                                     str(emocion).upper() + u"(F)")
                msql.registrar_resultado_campana(campana.id, str(emocion).lower(), 'female', cantidad_tweets)
            os.remove(archivo_coordenadas)

            # Se crea cada mapa de calor de la emoción y considerando los casos con género sin determinar
            datos = es.busqueda_heatmap(campana, emocion, 'unknown')
            nombre = str(campana.id) + "_" + emocion + "_unknown"
            archivo_coordenadas, cantidad_tweets = self.obtener_coordenadas(nombre, datos)
            if cantidad_tweets > 0:
                self.generar_heatmap(archivo_coordenadas, cantidad_tweets, nombre, u"Emoción: " +
                                     str(emocion).upper() + u"(?)")
                msql.registrar_resultado_campana(campana.id, str(emocion).lower(), 'unknown', cantidad_tweets)
            os.remove(archivo_coordenadas)

        logger.info(u"Se han creado los mapas de calor para la Campaña <id:%d>", campana.id)

    # Obtiene un archivo con las coordenadas necesarias para los mapas de calor
    def obtener_coordenadas(self, nombre, datos):
        ruta_base = os.path.abspath("")
        cantidad_tweets = 0
        # Se prepara la estructura del resultado entregado para dibujar en la vista
        for attribute, value in datos['hits'].iteritems():
            if attribute == 'hits':
                # Se genera el archivo de coordenadas "tweets.coords" en la ruta /app/reportes/heatmaps/
                file = open(ruta_base + "/app/reportes/heatmaps/" + nombre + ".coords", "w")
                for x in value:
                    for attr, val in x.iteritems():
                        if attr == '_source':

                            # Se escriben las coordenadas en el respectivo archivo
                            coordenadas = val['coordinates']
                            coordenadas.reverse()
                            for x in coordenadas:
                                file.write(" " + str(x))
                            file.write('\n')
                            cantidad_tweets += 1
                file.close()
        return ruta_base + "/app/reportes/heatmaps/" + nombre + ".coords", cantidad_tweets

    # Genera un mapa de calor considerando archivo de coordenadas y nombre de imagen
    def generar_heatmap(self, archivo_coordenadas, cantidad_tweets, nombre_imagen, titulo):
        ruta_base = os.path.abspath("")

        # Se crea imagen gradiente de color
        base_gradient = imread(ruta_base + "/app/reportes/gradient.png")
        base_gradient.shape

        hsva_min = Color()
        hsva_min.hex_l = '#b0c6e5'

        hsva_max = Color()
        hsva_max.hex_l = '#491849'

        color_gradient = list(hsva_max.range_to(hsva_min, base_gradient.shape[0]))
        alpha = np.arange(0, 256)[::-1]

        gradient = []
        for i, color_point in enumerate(color_gradient):
            rgb = list(color_point.get_rgb())
            rgb = [int(e * 255) for e in rgb]
            rgb.append(alpha[i])
            gradient.append([rgb])
        color_gradient = np.array(gradient)

        width = 43
        from copy import deepcopy

        color_gradient_row = deepcopy(color_gradient)

        for col in range(width - 1):
            color_gradient = np.hstack((color_gradient, color_gradient_row))

        imsave(ruta_base + "/app/reportes/gradient.png", color_gradient)

        # Se crea el mapa de calor con las coordenadas que se encuentran en el archivo tweets.coords
        try:
            heatmap = call(
                "python " + ruta_base + "/app/reportes/heatmap.py" + " --debug"
                + " -G " + ruta_base + "/app/reportes/gradient.png"
                + " -p " + archivo_coordenadas
                + " -W 1800"
                + " -o " + ruta_base + "/app/reportes/heatmaps/" + nombre_imagen + '.png'
                + " -P equirectangular"
                + " --osm --osm_base=http://a.basemaps.cartocdn.com/rastertiles/pitney-bowes-grey/"
                + " --decay 0.7"
                + " -v"
                + " -r 15",
                shell=True)
            if heatmap < 0:
                logger.error(u"Child was terminated by signal: %d", -heatmap)
            else:
                # Se agregan etiquetas de texto al mapa ya generado
                im = Image.open(ruta_base + "/app/reportes/heatmaps/" + nombre_imagen + '.png')
                width, height = im.size
                draw = ImageDraw.Draw(im)
                rel = int(width/30)
                font = ImageFont.truetype("/usr/share/fonts/truetype/tlwg/TlwgTypewriter-Bold.ttf", rel)
                draw.text((width -int(18*rel), height-6*int(rel)), titulo, (0, 0, 0), font=font)
                draw.text((width -int(18*rel), height-4*int(rel)), "Santiago, Chile (" + str(cantidad_tweets) + " Tweets)", (0, 0, 0), font=font)
                draw.text((width -int(18*rel), height-2*int(rel)), "geotendencias.cl", (30, 30, 30), font=font)

                im.save(ruta_base + "/app/reportes/heatmaps/" + nombre_imagen + '.png')
                logger.info(u"Child returned: %d", heatmap)
                return cantidad_tweets
        except OSError as e:
            logger.error(u"Execution failed:")
            logger.error(e)
