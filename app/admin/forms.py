# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, HiddenField
from wtforms.validators import DataRequired, Email
from .. models import User


class UsuarioForm(FlaskForm):
    id = HiddenField()
    username = StringField('Username',
                           validators=[
                               DataRequired("Debe ingresar un Username")
                           ])
    nombre = StringField('Nombre',
                         validators=[DataRequired("Debe ingresar el Nombre")])
    apellido = StringField('Apellido',
                           validators=[DataRequired("Debe ingresar el Apellido")])
    email = StringField('Email',
                        validators=[DataRequired(u"Debe ingresar un Email válido"),
                                    Email(u"Debe ingresar un Email con formato válido")])
    clave = PasswordField('Clave')
    confirmar_clave = PasswordField('Confirme la Clave')
    es_admin = BooleanField('Es Administrador')
    activo = BooleanField('Usuario Activo')
    submit = SubmitField('Guardar')

    def validate(self):
        validation = FlaskForm.validate(self)
        if not validation:
            return False

        if len(str(self.username.data).strip()) < 6:
            self.username.errors.append(u'El nombre de usuario debe tener al menos 6 caracteres')
            return False

        user_db = User.query.filter_by(username=self.username.data).first()
        # Si es un nuevo usuario
        if not self.id.data:
            if user_db and user_db.username == self.username.data:
                self.username.errors.append(u'El Username ya existe')
                return False

            if not self.clave.data:
                self.clave.errors.append(u'Debe ingresar una Clave')
                return False

            if not self.confirmar_clave.data:
                self.confirmar_clave.errors.append(u'Debe repetir la Clave')
                return False

            elif str(self.clave.data) != str(self.confirmar_clave.data):
                self.clave.errors.append(u'Las Claves deben ser iguales')
                self.confirmar_clave.errors.append(u'Las Claves deben ser iguales')
                return False

            if len(str(self.clave.data).strip()) < 6:
                self.clave.errors.append(u'Las Claves deben contener al menos 6 caracteres')
                self.confirmar_clave.errors.append(u'Las Claves deben contener al menos 6 caracteres')
                return False

        # Si está editando un usuario existente
        elif self.id.data:
            if user_db and int(user_db.id) != int(self.id.data):
                self.username.errors.append(u'No es posible modificar el Username')
                return False

            if (self.clave.data or self.confirmar_clave.data) and (str(self.clave.data) != str(self.confirmar_clave.data)):
                self.clave.errors.append(u'Las Claves deben ser iguales')
                self.confirmar_clave.errors.append(u'Las Claves deben ser iguales')
                return False

            if len(str(self.clave.data).strip()) < 6:
                self.clave.errors.append(u'Las Claves deben contener al menos 6 caracteres')
                self.confirmar_clave.errors.append(u'Las Claves deben contener al menos 6 caracteres')
                return False

        return True
