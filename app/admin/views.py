from flask import flash, redirect, render_template, url_for
from flask_login import current_user, login_required
from werkzeug.security import generate_password_hash
from app.utils import Utils
from app.persistencia import mysqlutils
from . import admin
from .. import db
from forms import UsuarioForm
from ..models import User

mysql = mysqlutils.Utils()

@admin.route('/admin/usuarios')
@login_required
def list_usuarios():
    Utils.check_admin()
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_admin']['estado'] = 'active'
    menu['treeview_admin']['usuarios']['estado'] = 'active'
    usuarios = User.query.all()
    return render_template('admin/usuarios/usuarios.html',
                           usuarios=usuarios, title='Usuarios', menu=menu, navbar_data=navbar_data)

@admin.route('/admin/usuarios/crear', methods=['GET', 'POST'])
@login_required
def crear_usuario():
    Utils.check_admin()
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_admin']['estado'] = 'active'
    menu['treeview_admin']['usuarios']['estado'] = 'active'

    crear_usuario = True

    form = UsuarioForm()
    if form.validate_on_submit():
        usuario = User(nombre=form.nombre.data,
                    apellido=form.apellido.data,
                    username=form.username.data,
                    email=form.email.data,
                    password_hash=generate_password_hash(form.clave.data),
                    es_admin=form.es_admin.data)

        try:
            db.session.add(usuario)
            db.session.commit()
            flash('Se ha creado un nuevo Usuario', 'success')
        except:
            flash('Error: no fue posible crear al Usuario', 'error')

        # Redirige al mantenedor de usuarios
        return redirect(url_for('admin.list_usuarios'))

    return render_template('admin/usuarios/usuario.html', crear_usuario=crear_usuario,
                           form=form, title='Crear Usuario', menu=menu, navbar_data=navbar_data)

@admin.route('/admin/usuarios/editar/<int:id>', methods=['GET', 'POST'])
@login_required
def editar_usuario(id):
    # Esto asegura que solo un admin o el propio usuario pueda modificar sus datos
    if current_user.id != id:
        Utils.check_admin()

    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_admin']['estado'] = 'active'
    menu['treeview_admin']['usuarios']['estado'] = 'active'

    crear_usuario = False

    usuario = User.query.get_or_404(id)
    form = UsuarioForm(obj=usuario)
    if form.validate_on_submit():
        usuario.nombre = form.nombre.data
        usuario.apellido = form.apellido.data
        usuario.email = form.email.data

        if form.clave.data:
            usuario.password_hash = generate_password_hash(form.clave.data)

        if current_user.es_admin:
            usuario.es_admin = form.es_admin.data
            usuario.activo = form.activo.data

        db.session.add(usuario)
        db.session.commit()
        flash('El usuario ha sido editado', 'success')

        if current_user.es_admin:
            # Redirige al mantenedor de usuarios
            return redirect(url_for('admin.list_usuarios'))
        else:
            return redirect(url_for('dashboard.index'))

    form.nombre.data = usuario.nombre
    form.apellido.data = usuario.apellido
    return render_template('admin/usuarios/usuario.html', crear_usuario=crear_usuario,
                           form=form, title="Editar Usuario", menu=menu, navbar_data=navbar_data)

