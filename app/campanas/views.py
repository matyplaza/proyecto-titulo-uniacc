# -*- coding: utf-8 -*-
import json
import os
from datetime import datetime
from flask import render_template, flash, url_for, redirect, send_file, abort
from flask_login import login_required, current_user
from app.persistencia import elasticsearchutils
from app.utils import Utils

from . import campanas
from .. import db
from forms import CampanaForm, AnalisisForm
from ..models import Campana
from app.persistencia import mysqlutils
from app.utils import EstadosCampanas

import logging
from logging.config import fileConfig
fileConfig('logging_config.ini')
logger = logging.getLogger()

mysql = mysqlutils.Utils()

@campanas.route('/campanas/descarga_heatmap/<string:nombre>', methods=['GET', 'POST'])
@login_required
def descarga_heatmap(nombre):
    nombre_imagen = nombre + ".png"
    ruta_base = os.path.abspath("")
    return send_file(ruta_base + "/app/reportes/heatmaps/" + nombre_imagen, as_attachment=True)


@campanas.route('/campanas/resultados_campana/<int:id>', methods=['GET', 'POST'])
@login_required
def resultados_campana(id):
    campana = Campana.query.filter(Campana.id == id).first()
    if campana is not None and campana.user_id != current_user.id:
        Utils.check_admin()
    elif campana is not None and campana.estado == EstadosCampanas().FINALIZADA:
        resultado = mysql.notificacion_leida(id)
        if resultado:
            logger.info(u"Se leyó la notificación de la Campaña: <ID:%d>", id)
        else:
            logger.info(u"No se pudo actualizar el estado de la notificación para la Campaña: <ID:%d>", id)
    elif campana is None:
        abort(404)
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_campanas']['estado'] = 'active'
    menu['treeview_campanas']['consultar']['estado'] = 'active'


    # Totales en cantidad
    totales_acumulados = mysql.get_totales_acumulados(id, False, False)

    # Se calcula el total de tweets
    total_tweets = 0
    for total_acumulado in totales_acumulados:
        total_tweets += total_acumulado

    # Totales en cantidad (género masculino)
    totales_acumulados_male = mysql.get_totales_acumulados(id, False, 'male')
    # Se calcula el total de tweets
    total_tweets_male = 0
    if totales_acumulados_male is not None:
        for total_acumulado_male in totales_acumulados_male:
            total_tweets_male += total_acumulado_male

    # Totales en cantidad (género femenino)
    totales_acumulados_female = mysql.get_totales_acumulados(id, False, 'female')
    # Se calcula el total de tweets
    total_tweets_female = 0
    if totales_acumulados_female is not None:
        for total_acumulado_female in totales_acumulados_female:
            total_tweets_female += total_acumulado_female

    # Totales en cantidad (género sin determinar)
    totales_acumulados_unknown = mysql.get_totales_acumulados(id, False, 'unknown')
    # Se calcula el total de tweets
    total_tweets_unknown = 0
    if totales_acumulados_unknown is not None:
        for total_acumulado_unknown in totales_acumulados_unknown:
            total_tweets_unknown += total_acumulado_unknown

    # Gráfico Polar
    datos_polar = totales_acumulados

    # Totales en %
    datos_piechart = mysql.get_totales_acumulados(id, True, False)

    # Totales en % (género masculino)
    datos_piechart_male = None if total_tweets_male == 0 else mysql.get_totales_acumulados(id, True, 'male')

    # Totales en % (género femenino)
    datos_piechart_female = None if total_tweets_female == 0 else mysql.get_totales_acumulados(id, True, 'female')

    # Totales en % (género indeterminado)
    # datos_piechart_unknown = None if total_tweets_unknown == 0 else mysql.get_totales_acumulados(id, True, 'unknown')
    datos_piechart_unknown = mysql.get_totales_acumulados(id, True, 'unknown')

    els = elasticsearchutils.Utils()
    parametros = {
        'hashtag': campana.hashtag,
        'screen_name': campana.screen_name,
        'fecha_inicio_es': campana.fecha_inicio.strftime('%a %b %d %H:%M:%S +0000 %Y'),
        'fecha_termino_es': campana.fecha_termino.strftime('%a %b %d %H:%M:%S +0000 %Y'),
    }

    resultados = els.busqueda_estandar(parametros)

    return render_template('campanas/resultados.html',
                           title=u'Resultado Campaña',
                           campana=campana,
                           id_campana=str(campana.id),
                           datos_polar=datos_polar,
                           datos_piechart=datos_piechart,
                           datos_piechart_male=datos_piechart_male,
                           datos_piechart_female=datos_piechart_female,
                           datos_piechart_unknown=datos_piechart_unknown,
                           comunas=resultados['comunas'],
                           totales=resultados['totales'],
                           total_tweets=total_tweets,
                           total_tweets_male=total_tweets_male,
                           total_tweets_female=total_tweets_female,
                           total_tweets_unknown=total_tweets_unknown,
                           totales_acumulados_male=totales_acumulados_male,
                           totales_acumulados_female=totales_acumulados_female,
                           totales_acumulados_unknown=totales_acumulados_unknown,
                           menu=menu,
                           navbar_data=navbar_data)


@campanas.route('/campanas/editar_campana/<int:id>', methods=['GET', 'POST'])
@login_required
def editar_campana(id):
    # Esto asegura que solo un admin o el propio usuario pueda modificar sus datos
    campana = Campana.query.get_or_404(id)
    if current_user.id != campana.user_id:
        Utils.check_admin()

    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_campanas']['estado'] = 'active'
    menu['treeview_campanas']['crear']['estado'] = 'active'

    crear_campana = False

    form = CampanaForm(obj=campana)
    if form.validate_on_submit():
        campana.nombre = form.nombre.data
        campana.descripcion = form.descripcion.data
        campana.hashtag = form.hashtag.data
        campana.screen_name = form.screen_name.data
        campana.fecha_inicio = form.fecha_inicio.data
        campana.fecha_termino = form.fecha_termino.data

        if datetime.now() >= form.fecha_termino.data:
            campana.estado = EstadosCampanas().PROCESANDO_RESULTADOS
        elif form.fecha_inicio.data <= datetime.now() < form.fecha_termino.data:
            campana.estado = EstadosCampanas().EN_CURSO
        else:
            campana.estado = EstadosCampanas().PROGRAMADA

        db.session.add(campana)
        db.session.commit()
        flash(u'La Campaña ha sido editada', 'success')

        return redirect(url_for('campanas.list_campanas'))

    return render_template('campanas/campana.html', crear_campana=crear_campana,
                           form=form, title=u"Editar Campaña", menu=menu, navbar_data=navbar_data)


@campanas.route('/campanas/borrar_campana/<int:id>', methods=['GET', 'POST'])
@login_required
def borrar_campana(id):
    try:
        campana = Campana.query.get_or_404(id)
        db.session.delete(campana)
        db.session.commit()
        flash(u'Se ha eliminado la Campaña', 'success')
    except:
        flash(u'Error: no fue posible eliminar la Campaña', 'error')
    return redirect(url_for('campanas.list_campanas'))


@campanas.route("/campanas/nueva", methods=['GET', 'POST'])
@login_required
def crear_campana():
    # Se cargan los datos para la navbar
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_campanas']['estado'] = 'active'
    menu['treeview_campanas']['crear']['estado'] = 'active'

    crear_campana = True

    form = CampanaForm()
    if form.validate_on_submit():

        if datetime.now() >= form.fecha_termino.data:
            estado = EstadosCampanas().PROCESANDO_RESULTADOS
        elif form.fecha_inicio.data <= datetime.now() < form.fecha_termino.data:
            estado = EstadosCampanas().EN_CURSO
        else:
            estado = EstadosCampanas().PROGRAMADA

        campana = Campana(
                    user_id=current_user.id,
                    nombre=form.nombre.data,
                    descripcion=form.descripcion.data,
                    hashtag=form.hashtag.data,
                    screen_name=form.screen_name.data,
                    fecha_inicio=form.fecha_inicio.data,
                    fecha_termino=form.fecha_termino.data,
                    finalizacion_notificada=False,
                    estado=estado)

        try:
            db.session.add(campana)
            db.session.commit()
            flash(u'Se ha creado una nueva Campaña', 'success')
        except:
            flash(u'Error: no fue posible crear una Campaña', 'error')

        # Redirige al mantenedor de Campañas
        return redirect(url_for('campanas.list_campanas'))

    return render_template('campanas/campana.html', crear_campana=crear_campana,
                           form=form, title=u'Crear Campaña', menu=menu, navbar_data=navbar_data)


@campanas.route("/campanas/gestion", methods=['GET', 'POST'])
@login_required
def list_campanas():
    navbar_data = mysql.get_navbar_data()
    menu = Utils.init_menu()
    menu['treeview_campanas']['estado'] = 'active'
    menu['treeview_campanas']['consultar']['estado'] = 'active'

    if current_user.es_admin == True:
        campanas = Campana.query.all()
    else:
        campanas = Campana.query.filter(Campana.user_id == current_user.id).all()


    return render_template('campanas/campanas.html',
                           campanas=campanas, title='Campanas', menu=menu, navbar_data=navbar_data)


@campanas.route("/campanas/analisis", methods=['GET', 'POST'])
@login_required
def analisis():
    navbar_data = mysql.get_navbar_data()
    # Se cargan todos los menus
    menu = Utils.init_menu()
    # Se definen los menus activos
    menu['treeview_campanas']['estado'] = 'active'
    menu['treeview_campanas']['analisis']['estado'] = 'active'

    form = AnalisisForm()
    if form.validate_on_submit():
        els = elasticsearchutils.Utils()
        parametros = {
            'hashtag': form.hashtag.data,
            'screen_name': form.screen_name.data,
            'fecha_inicio_es': form.fecha_inicio.data.strftime('%a %b %d %H:%M:%S +0000 %Y'),
            'fecha_inicio': form.fecha_inicio.data.strftime('%d/%m/%Y %H:%M'),
            'fecha_termino_es': form.fecha_termino.data.strftime('%a %b %d %H:%M:%S +0000 %Y'),
            'fecha_termino': form.fecha_termino.data.strftime('%d/%m/%Y %H:%M')
        }

        resultados = els.busqueda_estandar(parametros)

        return render_template('campanas/analisis.html',
                               parametros=parametros,
                               comunas=resultados['comunas'],
                               totales=resultados['totales'],
                               titulo_head=u'Resultado Análisis',
                               current_user=current_user,
                               menu=menu,
                               navbar_data=navbar_data
                               )
    return render_template('campanas/analisis.html',
                           form=form,
                           titulo_head=u'Análisis',
                           current_user=current_user,
                           menu=menu,
                           navbar_data=navbar_data
                           )
