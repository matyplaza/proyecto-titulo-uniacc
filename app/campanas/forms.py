# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from wtforms import StringField, DateTimeField, HiddenField, SubmitField
from wtforms.validators import DataRequired
from .. models import Campana


class CampanaForm(FlaskForm):
    nombre = StringField('Nombre',
                         validators=[DataRequired(u"Debe ingresar el Nombre de la campaña")],
                         render_kw={"placeholder": u"Nombre de Campaña"})
    descripcion = StringField(u'Descripción',
                         validators=[DataRequired(u"Debe ingresar la descripción de la Campaña")],
                         render_kw={"placeholder": u"Descripción de la Campaña"})
    hashtag = StringField('hashtag',
                          render_kw={"placeholder": u"Hashtag"})
    screen_name = StringField('screen_name',
                          render_kw={"placeholder": "Screen Name"})
    rango_fechas = StringField('Rango Fechas',
                          validators=[DataRequired("Debe ingresar un rango de fechas")],
                          render_kw={"placeholder": "Rango de fechas y horas"})
    fecha_inicio = DateTimeField('Fecha de Inicio',
                        format="%Y-%m-%d %H:%M:%S",
                        validators=[DataRequired("Debe ingresar una fecha de inicio")])
    fecha_termino = DateTimeField('Fecha de Termino',
                        format="%Y-%m-%d %H:%M:%S",
                        validators=[DataRequired(u"Debe ingresar una fecha de término")])
    submit = SubmitField('Guardar')

    def validate(self):
        validation = FlaskForm.validate(self)
        if not validation:
            return False

        if not self.hashtag.data and not self.screen_name.data:
            self.hashtag.errors.append(u'Debe ingresar un Hashtag o un Screen Name')
            self.screen_name.errors.append(u'Debe ingresar un Hashtag o un Screen Name')
            return False
        return True


class AnalisisForm(FlaskForm):
    hashtag = StringField('hashtag',
                          render_kw={"placeholder": "Hashtag"})
    screen_name = StringField('screen_name',
                          render_kw={"placeholder": "Screen Name"})
    rango_fechas = StringField('Rango Fechas',
                          validators=[DataRequired("Debe ingresar un rango de fechas")],
                          render_kw={"placeholder": "Rango de fechas y horas"})
    fecha_inicio = DateTimeField('Seleccione Fecha Inicio',
                             format="%Y-%m-%d %H:%M:%S",
                             validators=[DataRequired("Debe ingresar un rango de fechas")],
                             render_kw={"placeholder": "Desde el..."})
    fecha_termino = DateTimeField('Seleccione Fecha Término',
                              format="%Y-%m-%d %H:%M:%S",
                              validators=[DataRequired("Debe ingresar un rango de fechas")],
                              render_kw={"placeholder": "Hasta el..."})

    def validate(self):
        validation = FlaskForm.validate(self)
        if not validation:
            return False

        if not self.hashtag.data and not self.screen_name.data:
            self.hashtag.errors.append(u'Debe ingresar un Hashtag o un Screen Name')
            self.screen_name.errors.append(u'Debe ingresar un Hashtag o un Screen Name')
            return False
        return True
