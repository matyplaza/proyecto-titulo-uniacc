# README #

## Objetivo ##
* Aplicación para el análisis de sentimiento geolocalizado en Twitter
* Version 1.0

## Configuración Previa ##
* Se recomienda trabajar con virtualenv para desarrollo.
* Se deben crear las siguientes variables de ambiente en el SO utilizado, por ejemplo, agregando lo siguiente en el archivo ~/.bashrc
  * Ejecutar en terminal: `sudo vim ~/.bashrc`
  * Agregar las siguientes líneas al final del archivo (reemplazar por los valores reales a utilizar):
    * export FLASK_CONFIG="development"
    * export TWITTER_CONSUMER_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXX"
    * export TWITTER_CONSUMER_SECRET="XXXXXXXXXXXXXXXXXXXXXXXXXXX"
    * export TWITTER_ACCESS_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXX"
    * export TWITTER_ACCESS_SECRET="XXXXXXXXXXXXXXXXXXXXXXXXXXX"
  * Ejecutar por terminal: `source ~/.bashrc`

## Requisitos ##
* Se debe contar con las fuentes "fonts-tlwg-typewriter-ttf":
  *  sudo apt-get install fonts-tlwg-typewriter-ttf
* Se debe realizar la instalación de las librerías que siguen, escogiendo el método automático o el manual.
* Se debe instalar Java JDK (versión 8).
* Se debe contar con Elasticsearch corriendo en forma local (ejecución por defecto).
* Se debe ejecutar por terminal, el siguiente comando para realizar el mapeo de los índices en Elasticsearch:
  `curl -XPUT http://localhost:9200/_template/twitter -d '
    {
      "template" : "twitter*",
      "settings" : {
          "number_of_shards" : 1
      },
      "mappings" : {
        "default" : {
          "properties" : {
            "created_at" : {
              "type" : "date",
              "format" : "EEE MMM dd HH:mm:ss Z YYYY"
            }
          }
        }
      }
    }'`
* Si ya existía índice en Elasticsearch, se le debe ejecutar por terminal:
  `curl -XPOST  http://localhost:9200/_reindex -d '
    {
      "source": {
        "index": "nombre-indice-antiguo"
      },
      "dest": {
        "index": "twitter"
      }
    }'`
* Instalar MySQL: sudo apt-get install mysql-server
* Instalar mysql-config: sudo apt-get install libmysqlclient-dev
  * $ mysql -u root
  * mysql> CREATE USER 'gt_usuario_admin'@'localhost' IDENTIFIED BY 'gt20!I7';
  * mysql> CREATE DATABASE uniacc;
  * mysql> GRANT ALL PRIVILEGES ON uniacc . * TO 'gt_usuario_admin'@'localhost';

#### Instalación de librerías ####
* Se debe ejecutar en un terminal:
  * pip install -r requirements.txt

## Migración Base Datos ##
* En un terminal, ejecutar:
  * export FLASK_APP=wsgi_main.py
  * Caso 1: Luego de haber descargado nuevo código fuente desde el repositorio (con la siguiente línea actualizamos en base de datos automáticamente):
    * flask db upgrade
  * Caso 2: Luego de modificar nuestro código fuente del modelo (con las siguientes líneas creamos una nueva migración y actualizamos en base de datos automáticamente):  
    * flask db migrate
    * flask db upgrade
    
## Creación de Usuario Admin ##    
* En un terminal, ejecutar:
  * export FLASK_APP=wsgi_main.py
  * flask shell
    * from app.models import User
    * from app import db
    * admin = User(email="admin@admin.com",username="admin", password="admin123@",es_admin=True,activo=True)
    * db.session.add(admin)
    * db.session.commit()

## Ejecución ##
Situándose en el directorio raíz de la aplicación, se debe ejecutar en forma independiente (no necesitan correr en paralelo):
* Para correr la aplicación principal: `gunicorn --timeout 1200 -b 0.0.0.0:8080 wsgi_main:app`
* Para correr la captura de tweets: `gunicorn --timeout 1200 -b 0.0.0.0:9090 wsgi_listener:app2`
* Para correr la programación de tareas: `gunicorn --timeout 1200 -b 0.0.0.0:7070 wsgi_schedule:app3`
* En ambiente desarrollo pueden utilizar útiles los siguientes parámetros adicionales: `--reload  --log-level debug  --preload`

## Acceso ##
* Ingresar por navegador web a la URL: `http://[IP-MÁQUINA]:8080`